'use strict'

const {RegisterService} = require("../../../Services/RegisterService");

class RegisterController {
  async start({request, response}) {
    const {email, challenge} = request.all()

    const res = await RegisterService.start(email, challenge)

    return response.json(res)
  }

  async finish({request, response}) {
    const {email, pk, envelope} = request.all()

    const res = await RegisterService.finish(request.params.id, email, pk, envelope)

    return response.json(res)
  }
}

module.exports = RegisterController
