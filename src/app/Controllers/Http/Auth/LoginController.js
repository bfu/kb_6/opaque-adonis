'use strict'

const {LoginService} = require("../../../Services/LoginService");

class LoginController {
  async start({request, response}) {
    const {email, authId, challenge} = request.all()

    const res = await LoginService.start(email, authId, challenge)

    return response.json(res)
  }

  async finish({request, response}) {
    const {email, encryptKey, decryptKey} = request.all()

    const res = await LoginService.finish(request.params.id, email, encryptKey, decryptKey)

    return response.json(res)
  }
}

module.exports = LoginController
