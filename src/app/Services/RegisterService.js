const {OprfService} = require("./OprfService");
const {SodiumService} = require("./SodiumService");

let sodium = null

const UserAuth = use('App/Models/UserAuth')

class RegisterService extends SodiumService {

  static async start(email, challenge) {
    const {encodedPoint, oprfPk, oprfSk} = await OprfService.start(challenge)

    const userAuth = new UserAuth()

    sodium = await this.getSodium()

    const {
      publicKey,
      privateKey,
    } = sodium.crypto_kx_keypair()

    userAuth.email = email

    userAuth.pk = OprfService.encodePoint(publicKey)

    userAuth.sk = OprfService.encodePoint(privateKey)

    userAuth.oprf_pk = OprfService.encodePoint(oprfPk)

    userAuth.oprf_sk = OprfService.encodePoint(oprfSk)

    await userAuth.save()

    return {authId: userAuth.id, challenge: encodedPoint, oprfPk: userAuth.oprf_pk, pk: userAuth.pk}
  }

  static async finish(authId, email, userPk, envelope) {
    return await UserAuth
      .query()
      .where('id', authId)
      .where('email', email)
      .whereNull('user_pk')
      .whereNull('envelope')
      .update({
        envelope: envelope,
        user_pk: userPk
      })
  }
}

module.exports = {RegisterService}
