const {SodiumService} = require("./SodiumService");

let sodium = null

class OprfService extends SodiumService{
  static generateRandomScalar() {
    return sodium.crypto_core_ristretto255_scalar_random()
  }

  static isValidPoint(point) {
    return sodium.crypto_core_ristretto255_is_valid_point(point)
  }

  static scalarMult(point, key) {
    if (!this.isValidPoint(point)) {
      throw new Error('Not a valid Ristretto255 point.')
    }

    return sodium.crypto_scalarmult_ristretto255(key, point)
  }

  static hashToPoint(password) {
    const hash = sodium.crypto_generichash(
      sodium.crypto_core_ristretto255_HASHBYTES,
      sodium.from_string(password)
    )

    return sodium.crypto_core_ristretto255_from_hash(hash)
  }

  static maskPoint(point) {
    const r = this.generateRandomScalar()

    // maskedPoint = point * r
    const maskedPoint = this.scalarMult(point, r)

    return { r, maskedPoint }
  }

  static unmaskPoint(maskedPoint, mask) {
    // maskInv = mask ^ -1
    const maskInv = sodium.crypto_core_ristretto255_scalar_invert(mask)

    // maskedPoint * maskInv = maskedPoint * (mask ^ -1 )
    return this.scalarMult(maskedPoint, maskInv)
  }

  static encodePoint(point, encoding = 'ASCII') {
    const offset = encoding === 'ASCII' ? 1 : 2

    if (point.length % offset !== 0) {
      throw new Error('use ASCII!')
    }

    const code = []

    for (let i = 0; i < point.length; i += offset) {
      if (encoding === 'ASCII') {
        code[i] = point[i]
      } else {
        // UTF-8 (or rather USC-2) has 2 bytes per character
        code[i] = point[i] | (point[i + 1] << 8)
      }
      code[i] = String.fromCharCode(code[i])
    }

    return code.join('')
  }

  static decodePoint(code, encoding = 'ASCII') {
    const decode = []

    for (let i = 0; i < code.length; i++) {
      const character = code.charCodeAt(i)

      const decodeChar = []

      // Mask is not required for ASCII, but UTF-8 has second point encoded at 0xFF00
      // eslint-disable-next-line
      decodeChar.push(character & 0xFF)

      if (encoding !== 'ASCII') {
        // 2-byte characters, get second point
        decodeChar.push(character >> 8)
      }

      decode.push.apply(decode, decodeChar)
    }

    return Uint8Array.from(decode)
  }

  static async start(challenge) {
    sodium = await this.getSodium()

    let decodedPoint = this.decodePoint(challenge);

    // Server OprfSk: k
    const k = this.generateRandomScalar()

    // Server OprfPk: v = g * k
    const v = sodium.crypto_scalarmult_ristretto255_base(k)

    // beta = alpha * k
    const beta = this.scalarMult(decodedPoint, k)

    let encodedPoint = OprfService.encodePoint(beta)

    return {oprfPk: v, oprfSk: k, encodedPoint}
  }

  /**
   * responsePoint: beta
   * clientOprfSk: r
   * serverOprfPk: v
   */
  static async finish(oprfSk, challenge) {
    sodium = await this.getSodium()

    let decodedPoint = this.decodePoint(challenge);

    const beta = this.scalarMult(decodedPoint, this.decodePoint(oprfSk))

    let encodedPoint = OprfService.encodePoint(beta)

    return encodedPoint
  }
}


module.exports = {OprfService}
