const {OprfService} = require("./OprfService");
const {SodiumService} = require("./SodiumService");

let sodium = null

const UserAuth = use('App/Models/UserAuth')

class LoginService extends SodiumService {

  static async start(email, authId, challenge) {
    const userAuth = await UserAuth
      .query()
      .where('id', authId)
      .where('email', email)
      .whereNotNull('user_pk')
      .whereNotNull('envelope')
      .firstOrFail()

    const res = await OprfService.finish(userAuth.oprf_sk, challenge)

    return {
      envelope: userAuth.envelope,
      oprfPk: userAuth.oprf_pk,
      challenge: res
    }
  }

  static async finish(authId, email, encryptKey, decryptKey) {
    let userAuth = await UserAuth
      .query()
      .where('id', authId)
      .where('email', email)
      .whereNotNull('user_pk')
      .whereNotNull('envelope')
      .firstOrFail()

    console.log(userAuth)

    sodium = await this.getSodium()

    const {sharedRx, sharedTx} = sodium.crypto_kx_server_session_keys(
      OprfService.decodePoint(userAuth.pk),
      OprfService.decodePoint(userAuth.sk),
      OprfService.decodePoint(userAuth.user_pk),
    )

    encryptKey = new Uint8Array(Object.values(encryptKey))

    decryptKey = new Uint8Array(Object.values(decryptKey))

    return sodium.memcmp(encryptKey, sharedRx) && sodium.memcmp(decryptKey, sharedTx)
  }
}

module.exports = {LoginService}
