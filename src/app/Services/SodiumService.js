const _sodium = require('libsodium-wrappers-sumo')

class SodiumService {
  static async getSodium() {
    await _sodium.ready

    return _sodium
  }
}

module.exports = {SodiumService}

