'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserAuthSchema extends Schema {
  up() {
    this.create('user_auths', (table) => {
      table.increments()

      table.string('email', 255).notNullable()

      table.string('oprf_sk', 255).notNullable()
      table.string('oprf_pk', 255).notNullable()

      table.string('pk', 255).notNullable()
      table.string('sk', 255).notNullable()

      table.string('user_pk', 255).nullable()

      table.text('envelope', 'longtext').nullable()

      table.timestamps()
    })
  }

  down() {
    this.drop('user_auths')
  }
}

module.exports = UserAuthSchema
